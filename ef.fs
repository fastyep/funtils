open System
open System.Linq
open System.Reflection
open FSharp.Reflection
open Microsoft.EntityFrameworkCore
open Microsoft.EntityFrameworkCore.Metadata.Builders
open Newtonsoft.Json
let setProp (prop: string) (value: 'o -> 'v) (db: DbContext, obj: 'o) =
    obj.GetType().GetProperty(prop).SetValue(obj, value obj)
    db.Entry(obj).Property(prop).IsModified <- true
    (db, obj)

let setProp' (prop: string) (value: 'o -> 'v) (db: DbContext, obj: 'o option) =
    obj |> Option.map (fun obj -> (db, obj) |> setProp prop value) |> ignore
    (db, obj)

let setProps (db: DbContext) props o =
    let tt = try props.GetType() with :? NullReferenceException -> typeof<unit>
    if tt = typeof<unit> |> not then
        tt.GetProperties(BindingFlags.Instance ||| BindingFlags.Public)
        |> Seq.iter (fun p -> (db, o) |> setProp p.Name (fun _ -> FSharpValue.GetRecordField(props, p)) |> ignore)
    (db, o)        

let setProps' (db: DbContext) (pignore: string seq) props o =
    let tt = try props.GetType() with :? NullReferenceException -> typeof<unit>
    if tt = typeof<unit> |> not then
        tt.GetProperties(BindingFlags.Instance ||| BindingFlags.Public)
        |> Seq.filter (fun p -> pignore.Contains p.Name |> not)
        |> Seq.iter (fun p -> (db, o) |> setProp p.Name (fun _ -> FSharpValue.GetRecordField(props, p)) |> ignore)
    (db, o)        

let getProps (op: 'db -> 'o) (db: 'db) =
    (db :> DbContext, op db)

let safeSave (db: DbContext) =
    try
        db.SaveChanges() |> Ok
    with
    | :? DbUpdateException as ex when (ex.InnerException <> null) -> Error ex.InnerException
    | ex -> Error ex

let saveProps (db: DbContext, o: 'o) =
    safeSave db |> Result.map(fun _ -> o)

let jsonb<'a> (prop: PropertyBuilder<'a>) =
        prop.HasConversion(<@ fun (obj: 'a) -> JsonConvert.SerializeObject obj @> |> toLinq,
                           <@ fun (a: string) -> JsonConvert.DeserializeObject<'a> a @> |> toLinq)
            .HasColumnType "jsonb"