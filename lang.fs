open System
open System.Reflection
open System.Linq.Expressions
open System.Collections.Generic
open FSharp.Reflection
open FSharp.Linq.RuntimeHelpers
let same a =
    a

let invoke a f =
    f a

let tup a b =
    a, b

let tuper (kvp: KeyValuePair<'T, 'K>) =
    kvp.Key, kvp.Value

let pass (f: 'a -> 'b) (a: 'a) =
    f a |> ignore
    a

let lezy a =
    fun _ -> a

let opt v =
    if obj.Equals(v, null) then None else Some v

let tryOpt (ok: bool, result) =
    if ok then Some result else None

let tryDef (def: 'K) (f: 'T -> 'K) (a: 'T) =
    try f a with _ -> def

let toTuple<'s, 't> (l : seq<'s>) =
    let l' = l |> Seq.cast<obj> |> Seq.toArray
    let types = l' |> Array.map (fun o -> o.GetType())
    let tupleType = FSharpType.MakeTupleType types
    FSharpValue.MakeTuple (l' , tupleType) :?> 't

let toRecord<'T> o =
    let fields = o.GetType().GetProperties(BindingFlags.Instance ||| BindingFlags.Public)
    let mapper (p: PropertyInfo) =
        match fields |> Seq.tryFind (fun f -> f.Name.ToLower() = p.Name.ToLower() && p.PropertyType = f.PropertyType) with
        | Some f -> f.GetValue(o)
        | None -> null
    let arr = FSharpType.GetRecordFields typeof<'T> |> Seq.map mapper |> Seq.cast<obj> |> Seq.toArray
    FSharpValue.MakeRecord(typeof<'T>, arr) :?> 'T

let toLinq (expr : Quotations.Expr<'a -> 'b>) =
    let linq = LeafExpressionConverter.QuotationToExpression expr
    let call = linq :?> MethodCallExpression
    let lambda = call.Arguments.[0] :?> LambdaExpression
    Expression.Lambda<Func<'a, 'b>>(lambda.Body, lambda.Parameters)

let dics (key: 'k, value: 'v) (dic: IDictionary<'k, 'v>) =
    if isNull dic then Seq.empty
    else dic |> Seq.filter(fun a -> a.Key <> key)
    |> Seq.append [KeyValuePair.Create(key, value)]
    |> Dictionary
    :> IDictionary<'k, 'v>

let tryParse (parser: string -> bool * 'a) (str: string option) =
    str |> Option.map (fun str -> parser str |> fun (ok, v) -> if ok then Some v else None) |> Option.defaultValue None

let apdirt a =
    Array.append [|a|] >> Array.distinct >> Array.sort    

let setval (prop: string) (o: obj) (v: obj) =
    o.GetType().GetProperty(prop).SetValue(o, v)