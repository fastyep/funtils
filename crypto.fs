open System
open System.Text
open System.Security.Cryptography
let randomInt64() =
    Array.zeroCreate<byte> 8 |> fun bytes ->
        Random().NextBytes(bytes)
        BitConverter.ToInt64(bytes, 0)

let md5Raw (data: byte []) =
    use provider = new MD5CryptoServiceProvider()
    provider.ComputeHash(data)

let toHex (data: byte []) =
    BitConverter.ToString(data).Replace("-", "").ToLower()
    
let md5String (str: string) =
    Encoding.Unicode.GetBytes str |> md5Raw |> toHex

let fromHex (hex: string) =
    [0 .. hex.Length - 1]
    |> Seq.filter (fun x -> x % 2 = 0)
    |> Seq.map (fun x -> Convert.ToByte(hex.Substring(x, 2), 16))
    |> Seq.toArray

let getSaltedHash (pass: string) =
    Array.zeroCreate<byte> 4 |> fun salt ->
        Random().NextBytes(salt)
        Encoding.Unicode.GetBytes pass |> md5Raw
        |> Seq.append salt |> Seq.toArray |> md5Raw
        |> Seq.append salt |> Seq.toArray |> toHex

let chekSaltedHash (hash: string) (pass: string) =
    hash.Substring(0, 8) |> fromHex |> fun salt ->
        Encoding.Unicode.GetBytes pass |> md5Raw
        |> Seq.append salt |> Seq.toArray |> md5Raw |> toHex = hash.Substring(8)

let private aes = Aes.Create()

let fastEncrypt (text: string) =
    let arr = Encoding.ASCII.GetBytes text
    use enc = aes.CreateEncryptor()
    enc.TransformFinalBlock(arr, 0, arr.Length) |> toHex

let fastDecrypt (text: string) =
    let arr = fromHex text
    use enc = aes.CreateDecryptor()
    enc.TransformFinalBlock(arr, 0, arr.Length) |> Encoding.ASCII.GetString
    
let fastEncrypt' (bytes: byte array) =
    use enc = aes.CreateEncryptor()
    enc.TransformFinalBlock(bytes, 0, bytes.Length)

let fastDecrypt' (code: byte array) =
    use enc = aes.CreateDecryptor()
    enc.TransformFinalBlock(code, 0, code.Length)

let toBaseIds (ids: int array) =
    ids |> Seq.map BitConverter.GetBytes |> Seq.concat |> Seq.toArray |> Convert.ToBase64String

let fromBaseIds (str: string) =
    let arr = Convert.FromBase64String str
    [ 0 .. arr.Length / 4 - 1 ] |> Seq.map(fun i -> BitConverter.ToInt32(arr, i * 4)) |> Seq.toArray