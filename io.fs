open System
open System.Diagnostics
let runProc filename args startDir (handler: Result<string, string> -> unit) =
    let info = 
        ProcessStartInfo(
            RedirectStandardOutput = true,
            RedirectStandardError = true,
            UseShellExecute = false,
            FileName = filename,
            Arguments = args
        )
    info.StandardOutputEncoding <- Encoding.UTF8
    info.StandardErrorEncoding <- Encoding.UTF8
    match startDir with | Some d -> info.WorkingDirectory <- d | _ -> ()
    let p = new Process(StartInfo = info)
    let handleWith f (args:DataReceivedEventArgs) =
        if String.IsNullOrEmpty args.Data |> not then
            f args.Data |> handler |> ignore
    p.OutputDataReceived.Add <| handleWith Ok
    p.ErrorDataReceived.Add <| handleWith Error
    let started = 
        try
            p.Start()
        with | ex ->
            ex.Data.Add("filename", filename)
            reraise()
    if not started then
        failwithf "Failed to start process %s" filename
    p.BeginOutputReadLine()
    p.BeginErrorReadLine()
    p.WaitForExit()

let rec askConsole nani =
    printf "%s [y/n]? " nani
    let char = Console.ReadKey().Key
    Console.WriteLine()
    match char with
    | ConsoleKey.Y -> true
    | ConsoleKey.N -> false
    | _ -> askConsole nani

let logConsole (s: Result<string, string>) =
    match s with
    | Ok s -> printfn "%s" s
    | Error s ->
        Console.ForegroundColor <- ConsoleColor.Red
        Console.Error.Write s
        Console.ResetColor()