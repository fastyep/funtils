open System.Reflection
open FSharp.Reflection
let ipToInt (ip: string) =
    try
        ip.Split('.')
        |> Seq.mapi (fun i elem -> (elem |> int) * (1 <<< (8 * (3 - i))))
        |> Seq.sum
    with
        | _ -> 0

let httpQuery (p: obj) (url: string) =
    let mutable url = url.Replace('\\', '/')
    let tt = p.GetType();
    let dic = if tt = typeof<unit> then [||] else tt.GetProperties(BindingFlags.Instance ||| BindingFlags.Public)
                |> Seq.map (fun x -> (x.Name, url.Contains("/:" + x.Name), FSharpValue.GetRecordField(p, x)))
    let query = dic
                |> Seq.filter (fun (_, x, _) -> not x)
                |> Seq.map (fun (key, _, value) -> value.ToString() |> sprintf "%s=%s" key)
                |> String.concat "&"
                |> fun query -> if String.IsNullOrEmpty query then "" else "?" + query
    dic |> Seq.filter (fun (_, x, _) -> not x)
        |> Seq.iter (fun (key, _, value) -> url <- url.Replace(":" + key, value.ToString()))
    url + query