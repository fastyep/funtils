let (|LessThan|_|) k value =
    if value < k then Some() else None

let (|MoreThan|_|) k value =
    if value > k then Some() else None

let (|InSeq|_|) seq value =
    if seq |> Seq.contains value then Some() else None

let (|StartsWith|_|) (sub: string) (str: string) =
    if str.StartsWith sub then str.Substring sub.Length |> fun str -> if str.Length = 0 then None else Some str
    else None

let (|Search|_|) (str : string) =
    if isNull str then None
    else
        match "%" + str.Trim().Replace("%", "") + "%" with
        | "%%" -> None
        | str -> Some str

let (|ParseWith|_|) (parse: string -> bool * 'a) (str: string) =
    let ok, value = parse str
    if ok then Some value else None