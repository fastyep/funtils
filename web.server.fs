open Giraffe
open Microsoft.AspNetCore.Http
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Cors.Infrastructure
open Microsoft.AspNetCore.Hosting
open Microsoft.EntityFrameworkCore
open Microsoft.Extensions.Logging
open Microsoft.Extensions.DependencyInjection
open System

let errorHandler txt (ex: Exception) (logger: ILogger) =
    logger.LogError(ex, txt)
    clearResponse >=> setStatusCode 500 >=> text ex.Message
    
let corsHandler cors (builder: CorsPolicyBuilder) =
    builder.WithOrigins(cors)
           .AllowAnyMethod()
           .AllowAnyHeader()
           .AllowCredentials()

let cookiesHandler (options: CookiePolicyOptions) =
    options.CheckConsentNeeded <- fun _ -> true
    options.MinimumSameSitePolicy <- SameSiteMode.None
    options.Secure <- CookieSecurePolicy.Always
    options

let logHandler (builder: ILoggingBuilder) =
    builder.AddFilter(fun l -> l.Equals LogLevel.Error)
           .AddConsole()
           .AddDebug()

let withError (e: ErrorHandler) (host: IWebHostBuilder) =
    host, fun (app: IApplicationBuilder) -> app.UseGiraffeErrorHandler(e)
        
let withService s (host: IWebHostBuilder) =
    s >> ignore |> host.ConfigureServices

let withCors c (host: IWebHostBuilder, a: IApplicationBuilder -> IApplicationBuilder) =
    host.ConfigureServices(fun a -> a.AddCors() |> ignore), a >> fun app -> c >> ignore |> app.UseCors

let withWebSockets f (host: IWebHostBuilder, a: IApplicationBuilder -> IApplicationBuilder) =
    let opt = WebSocketOptions()
    opt.ReceiveBufferSize <- 4096
    opt.KeepAliveInterval <- TimeSpan.FromSeconds 120.0
    host, a >> fun app -> f opt |> ignore |> app.UseWebSockets

let withRoutes (r: HttpHandler) (host: IWebHostBuilder, a: IApplicationBuilder -> IApplicationBuilder) =
    host.Configure(a >> (fun app -> app.UseGiraffe r) >> ignore)
    |> withService (fun a -> a.AddGiraffe())

let withDbContext<'C when 'C :> DbContext> (c: DbContextOptionsBuilder -> DbContextOptionsBuilder) =
    withService(fun s -> c >> ignore |> s.AddDbContext<'C>)

let withCookies c =
    withService(fun s -> c >> ignore |> s.Configure)

let withSingleton (o: 'T) =
    withService (fun s -> s.AddSingleton<'T> o)

let withScoped (services: Type seq) =
    withService (fun c ->
        services
        |> Seq.iter (fun s -> c.AddScoped s |> ignore) )

let withLog l (host: IWebHostBuilder) =
    l >> ignore |> host.ConfigureLogging
    
let runOn (e: string) (host: IWebHostBuilder) =
    host.UseUrls(e).Build().Run()
    0