open Giraffe
open Microsoft.AspNetCore.Http
open FSharp.Reflection
open System.Text.Json
open Microsoft.EntityFrameworkCore
        
let keyItem (ctx: HttpContext) (t: Type) =
    if t = typeof<obj> && ctx.Items.ContainsKey typeof<Type>
    then ctx.Items.Item typeof<Type> :?> Type else t
    |> fun t -> ctx.Items.ContainsKey t, t
    
let import<'T> (ctx: HttpContext) =
    let minimport (t: Type) =
        keyItem ctx t
        |> fun (ok, tt) ->
            if ok then ctx.Items.Item tt
            else ctx.RequestServices.GetService t
    let t = typeof<'T>
    if FSharpType.IsTuple t then
        FSharpType.GetTupleElements t
        |> Array.map minimport
        |> fun a -> a, t
        |> FSharpValue.MakeTuple
    else minimport t
    |> function
    | :? 'T as res -> res
    | _ -> Unchecked.defaultof<'T>

let export (ctx: HttpContext) (a: 'T) =
    if obj.Equals(a, null) |> not then
        ctx.Items.[typeof<Type>] <- typeof<'T>
        ctx.Items.[ctx.Items.[typeof<Type>]] <- a
    
let inject<'T> (handler: 'T -> HttpHandler) (next: HttpFunc) (ctx: HttpContext) = 
    handler
    <| import<'T> ctx
    <| next
    <| ctx
    
let view (func: 'T -> 'V) (next: HttpFunc) (ctx: HttpContext) =
    import<'T> ctx
    |> func
    |> export ctx
    next ctx
    
let validationHandler<'T> (validation: 'T -> bool) (invalid: HttpHandler) (next: HttpFunc) (ctx: HttpContext) =
    if import<'T> ctx |> validation then invalid next ctx else next ctx 
    
let bindParseHandler (parse: string -> bool * 'T) (from: string) (def: 'T) (next: HttpFunc) (ctx: HttpContext) =
    ctx.TryGetQueryStringValue from
    |> tryParse parse
    |> Option.defaultValue def
    |> export ctx
    next ctx
    
let lastsyncHandler : HttpHandler =
    bindParseHandler DateTime.TryParse "sync" DateTime.MinValue
    
let bindQueryHandler<'T> (next: HttpFunc) (ctx: HttpContext) =
    ctx.BindQueryString<'T>()
    |> export ctx
    next ctx
    
let bindBodyHandler (next: HttpFunc) (ctx: HttpContext) =
    ctx.ReadBodyFromRequestAsync()
    |> Async.AwaitTask
    |> Async.RunSynchronously
    |> export ctx
    next ctx
    
let bindJsonHandler<'T> (next: HttpFunc) (ctx: HttpContext) =
    ctx.BindJsonAsync<'T>()
    |> Async.AwaitTask
    |> Async.RunSynchronously
    |> export ctx
    next ctx
    
let bindJsonlHandler<'T> (next: HttpFunc) (ctx: HttpContext) =
    ctx.ReadBodyFromRequestAsync()
    |> Async.AwaitTask
    |> Async.RunSynchronously
    |> fun s -> s.Split '\n'
    |> Array.choose (tryDef None (JsonSerializer.Deserialize<'T> >> Some) )
    |> export ctx
    next ctx
        
let saveDbHandler<'T when 'T :> DbContext> (next: HttpFunc) (ctx: HttpContext) =
    try ctx.GetService<'T>().SaveChanges() |> ignore; None
    with
    | :? DbUpdateException as ex when (isNotNull ex.InnerException) -> Some ex.InnerException
    | ex -> Some ex
    |> function
    | None -> next ctx
    | Some e -> RequestErrors.badRequest (text e.Message) next ctx